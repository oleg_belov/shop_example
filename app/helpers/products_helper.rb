module ProductsHelper
  def number_to_roubles(number)
    number_to_currency(number,
                       unit: '₽',
                       precision: 2,
                       format: '%n %u',
                       separator: ',',
                       delimiter: ' ')
  end
end
