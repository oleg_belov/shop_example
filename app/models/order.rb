class Order < ApplicationRecord
  has_many :line_items, dependent: :destroy
  has_many :products, through: :line_items

  def product_quantity(product_id)
    line_items.find_by(product_id: product_id).quantity
  rescue
    0
  end

  def sub_total
    sum = 0
    line_items.each do |line_item|
      sum += line_item.total_price
    end
    sum
  end
end
