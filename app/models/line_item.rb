class LineItem < ApplicationRecord
  belongs_to :cart, optional: true
  belongs_to :order, optional: true
  belongs_to :product

  validate :belongs_present?

  def total_price
    quantity * product.price
  end

  private

  def belongs_present?
    errors.add(:belongs, 'Order or Cart was not present!') unless
      order.present? || cart.present?
  end
end
