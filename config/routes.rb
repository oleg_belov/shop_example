Rails.application.routes.draw do

  get 'carts/show'

  root 'products#index'

  get 'cart' => 'carts#show', as: 'cart'
  delete 'cart' => 'carts#destroy'
  get 'checkout' => 'orders#create_fast'
  get 'products' => redirect('/')

  post 'line_items/:id/add' => 'line_items#add_quantity', as: 'line_item_add'
  post 'line_items/:id/reduce' => 'line_items#reduce_quantity', as: 'line_item_reduce'
  post 'line_items' => 'line_items#create'
  get 'line_items/:id' => 'line_items#show', as: 'line_item'
  delete 'line_items/:id' => 'line_items#destroy'

  resources :orders
  resources :products

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
