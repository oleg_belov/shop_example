class CreateLineItems < ActiveRecord::Migration[5.1]
  def change
    create_table :line_items do |t|
      t.integer :cart_id
      t.integer :order_id
      t.integer :product_id
      # t.references :cart, foreign_key: true
      # t.references :order, foreign_key: true
      # t.references :product, foreign_key: true
      t.integer :quantity, default: 1

      t.timestamps
    end
  end
end
